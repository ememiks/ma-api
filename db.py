import mysql.connector
from flask import current_app as app

def add_user(login, passwd):
	query = ("INSERT INTO users (login, passwd, version) VALUES('%s', '%s', 1)" % (login, passwd))
	execute_query(query)

def add_product(product_name, device_id):
	query = ("INSERT INTO list (product_name, quantity, device_id) VALUES ('%s', '%s', '%s')" % (product_name, 0, device_id))
	execute_query(query)
	query = ("SELECT id FROM list WHERE product_name='%s' and device_id='%s'" % (product_name, device_id))
	result = execute_query_with_respose(query)
	print result
	return result['results'][0]['id']

def get_products(list_id = 1):
	query = ("SELECT DISTINCT 0 as id, product_name, sum(quantity) as 'quantity', 1 as list_id FROM list GROUP BY product_name")
	data = execute_query_with_respose(query)
	return data

def change_quantity(product_name, quantity, device_id):
	query = ("SELECT * FROM list WHERE product_name='%s' and device_id='%s'" % (product_name, device_id))
	result = execute_query_with_respose(query)
	app.logger.info(result)
	if len(result['results']) == 0:
		add_product(product_name, device_id)
	query = ("UPDATE list SET quantity=%d WHERE product_name='%s' and device_id='%s'" % (quantity, product_name, device_id))
	execute_query(query)

def delete_product(product_name):
	query = ("DELETE FROM list WHERE product_name='%s'" % (product_name))
	execute_query(query)

def verify_user_data(login, passwd):
	query = ("SELECT passwd FROM users WHERE login='%s'" % login)
	result = execute_query_with_respose(query)
	app.logger.info(result)
	if len(result['results']) == 0:
		app.logger.info('No data')
		return False

	elif result['results'][0]['passwd'] == passwd:
		app.logger.info('Correct')
		return True
	else:
		app.logger.info('Wrong (%s:%s)' % (passwd, str(result[0])))
		return False

def execute_query(query):
	app.logger.info('EXECUTING SQL QUERY: ' + query)

	cnn = mysql.connector.connect(user='ememiks', password='api', host='ememiks.mysql.pythonanywhere-services.com', database='ememiks$ma')
	cursor = cnn.cursor()
	try:
		cursor.execute(query)
		cnn.commit()
	except Exception, e:
		app.logger.error(e)
		raise e

def execute_query_with_respose(query):
	app.logger.info('EXECUTING SQL QUERY WITH RESPONSE: ' + query)

	cnn = mysql.connector.connect(user='ememiks', password='api', host='ememiks.mysql.pythonanywhere-services.com', database='ememiks$ma')
	cursor = cnn.cursor()
	try:
		cursor.execute(query)
		data = {"results": [dict(zip([column[0] for column in cursor.description], row)) for row in cursor.fetchall()]}
		return data
	except Exception, e:
		app.logger.error(e)
		raise e