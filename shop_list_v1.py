from flask import Blueprint, request, abort, jsonify
import validictory
import json
import string
import logging
import db
import wrappers
from flask import current_app as app
from flask.ext.httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username, password):
    return db.verify_user_data(username, password)

sl = Blueprint('sl', __name__,)

@sl.route('/')
def get():
	return "Shop list v1 is working!"

@sl.route('/user/add', methods=['POST'])
@auth.login_required
@wrappers.json
def add_user():
	data = request.json
	app.logger.debug('Adding new user. Received JSON: ' + str(data))
	try:
		db.add_user(data["login"], data["passwd"])
	except Exception, e:
		msg = 'Adding user failed! Error message: ' + str(e)
		return jsonify({"error": msg}), 400
	return jsonify({'success':True}), 200, {'ContentType':'application/json'}

@sl.route('/product/add', methods=['POST'])
@wrappers.json
@auth.login_required
def add_product():
	data = request.json
	app.logger.info('Adding new product. Received JSON: ' + str(data))
	try:
		id = db.add_product(data['product_name'], data['device_id'])
		return str(id)
	except Exception, e:
		msg = 'Adding product failed! Error message: ' + str(e)
		return jsonify({"error": msg}), 400
	return jsonify({'success':True}), 200, {'ContentType':'application/json'}


@sl.route('/product/<product_name>', methods=['DELETE'])
@auth.login_required
def delete_product(product_name):
	data = request.json
	app.logger.info('Delete product [ID: %s].' % (product_name))
	try:
		db.delete_product(product_name)
	except Exception, e:
		msg = 'Deleting product failed! Error message: ' + str(e)
		return jsonify({"error": msg}), 400

	return jsonify({'success':True}), 200, {'ContentType':'application/json'}


@sl.route('/product/<product_name>', methods=['PUT'])
@wrappers.json
@auth.login_required
def change_product_quantity(product_name):
	data = request.json
	app.logger.debug('Change quantity [Product: %s]. Received JSON: %s' % (product_name, str(data)))
	try:
		db.change_quantity(product_name, data['change'], data['device_id'])
	except Exception, e:
		msg = 'Changing quantity failed! Error message: ' + str(e)
		return jsonify({"error": msg}), 400
	return jsonify({'success':True}), 200, {'ContentType':'application/json'}


@sl.route('/products', methods=['GET'])
@wrappers.json
@auth.login_required
def get_products():
	try:
		data = db.get_products()		
		app.logger.debug('Retriving products. Received data: ' + str(data))
	except Exception, e:
		msg = 'Retriving products failed! Error message: ' + str(e)
		return jsonify({"error": msg}), 400
	return jsonify(data), 200, {'ContentType':'application/json'}