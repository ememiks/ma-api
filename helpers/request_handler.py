import time
from werkzeug.serving import BaseRequestHandler

class ApiRequestHandler(BaseRequestHandler):
    """Extend request handler."""
    def handle(self):
        self.request_started = time.time()
        rv = super(ApiRequestHandler, self).handle()
        return rv

    def send_response(self, *args, **kw):
        self.request_completed = time.time()
        super(ApiRequestHandler, self).send_response(*args, **kw)

    def log_request(self, code='-', size='-'):
        duration = int((self.request_completed - self.request_started) * 1000)
        self.log('info', '"{0}" {1} {2} [{3}ms]'.format(self.requestline, code, size, duration))