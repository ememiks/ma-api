from functools import wraps
from flask import (
    current_app,
    jsonify,
    request,
    abort
)
from wtforms import ValidationError, validators
import validictory
from werkzeug.exceptions import HTTPException, BadRequest
import db

def json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except BadRequest, e:
            msg = "Data must be a valid JSON! Error: " + str(e)
            return jsonify({"error": msg}), 400
        return f(*args, **kw)
    return wrapper

