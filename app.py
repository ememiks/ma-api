from flask import Flask
from flask import current_app
from helpers import request_handler
from shop_list_v1 import sl
import logging
import sys

reload(sys)  
sys.setdefaultencoding('utf8')

app = Flask(__name__)
app.register_blueprint(sl, url_prefix="/api/v1/sl")

@app.route('/')
def index():
	app.logger.info("Hello, World!")
	return "Hello, World!"

if __name__ == '__main__':
	app.logger_name='ma-api'

	handler = logging.FileHandler('logs/api.log')
	formatter = logging.Formatter('%(asctime)-15s [%(levelname)6s] %(module)10s:%(lineno)04d:  %(message)s')

	handler.setFormatter(formatter)

	werkzeug_log = logging.getLogger('werkzeug')
	werkzeug_log.setLevel(logging.DEBUG)
	werkzeug_log.addHandler(handler)

	app.logger.addHandler(handler)
	app.logger.info('Welcome to ma-api!')

	app.run(host='192.168.56.180', debug=True, request_handler = request_handler.ApiRequestHandler)